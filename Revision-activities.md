****#### 🤷‍♀️ 开源指北是什么？

开源指北是一本完全由开源爱好者、开源社区、开源领域专家编写的开源入门电子书籍，在 52 位开源贡献者和 6 位开源领域专家的共同努力下，开源指北 1.0 已正式进入修订的阶段，欢迎更多爱好开源的小伙伴加入我们的修订工作中！

[开源指北项目主页>>](https://gitee.com/opensource-guide)

#### ⏱ 修订时间

2020 年 12 月 10 日 - 12 月 20 日

#### ❓ 修订规则

你可以通过 **轻量级 PR**  或  **Fork+ PR**  两种方式修订
**修订内容：**

- 根据修订任务对该章节进行补充或修改
- 若你对该章节有更深的理解，也欢迎进行其他内容补充

#### 🏆 参与修订，你可以获得

1. 一次亲身参与开源项目的经历，并成为 Gitee 官方开源电子文档编写成员（将在开源指北电子书“编写成员”栏署名，若为优秀内容贡献者，将颁发纸质证书）

2. 优秀贡献者（提交 PR 内容优质且符合规则，并被评审团合并）可获得纪念 T 恤一件

3. [贡献榜](https://gitee.com/gitee-community/opensource-guide/contributors?ref=master) 前 10 名（前三位官方维护人员除外）可获得 TiDB 开发文具礼盒 一份

#### 💡 快速引导

有任何编写相关问题可联系 Gitee 小助手咨询，完成修订后可联系小助手加入开源指北维护群，发放奖品，领取证书。
添加 Gitee 小助手（gitee2013），备注「开源指北」
![输入图片说明](https://images.gitee.com/uploads/images/2020/0712/212657_b00725ef_1899542.png "150-小助手微信.png")

### 👩‍👩‍👧‍👦 审校专家团队

为保证电子文档的内容更有参考价值，Gitee 内容组特别邀请了几位技术专家担任本次的审校人员（以下名单按首字母排序）：

- 代立冬
易观大数据平台总监，负责每日数百亿条数据处理链条的架构设计，技术选型，技术攻关等工作。十分热爱开源，是大数据任务调度 - Apache DolphinScheduler 的 PPMC & Committer，也活跃于各个大数据开源社区。
- 姜宁
华为开源能力中心技术专家，Apache 软件基金会会员，前红帽软件主任软件工程师，在企业级开源中间件开发方面有十余年经验，有丰富的 Java 开发和使用经验，函数式编程爱好者。从 2006 年开始一直从事 Apache 开源中间件项目的开发工作，先后参与 Apache CXF，Apache Camel，以及 Apache ServiceMix 的开发。
- 李建盛（笔名适兕）
李建盛，开源文化布道师，技术翻译，开源之道作者，opensourceway.community 创始人、作家、译者、开源社成员。
- 潘娟
京东数科高级 DBA，Apache ALC Beijing member，Apache ShardingSphere PMC 主要负责京东数科分布式数据库开发、数据库运维自动化平台开发等工作。曾负责京东数科数据库自动化平台设计与开发，现专注于 Apache ShardingSphere 分布式数据库中间件生态的架构及研发。主要在分布式数据库、开源、分布式架构等相关领域进行探索。多次受邀参加数据库&架构领域的相关会议并进行分享交流。
- 卫剑钒
开源“圣经”《大教堂与集市》中文版译者，国际信息系统安全认证专家（CISSP），目前就职于某大型金融机构，从事信息科技管理工作。曾发表过《开源的 7 大理念》、《从契约精神谈 MIT 协议》、《copyright 的真正含义是什么》、《步进式解读 Apache 许可证》、《使用 Apache 协议的是自由软件吗》等文章。
- 张亮
京东数科数字技术中心架构专家，Apache ShardingSphere，ElasticJob 创始人。热爱开源，擅长以 Java 为主分布式架构，推崇优雅代码。
目前主要精力投入在将分布式数据库中间件 Apache ShardingSphere 打造为业界一流的金融级数据解决方案之上。
Apache ShardingSphere 是京东主导的首个 Apache 软件基金会顶级项目，也是 Apache 软件基金会首个分布式数据库中间件。曾出版书籍《未来架构——从服务化到云原生》。

#### 🎪 支持社区

![输入图片说明](https://images.gitee.com/uploads/images/2020/1126/135201_1f86cd37_1899542.png "支持社区-11-26.png")

### License

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="知识共享许可协议" style="border-width:0" src="https://images.gitee.com/uploads/images/2020/0907/190809_d6adca86_5694891.png" /></a><br />本作品采用<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">知识共享署名 4.0 国际许可协议</a>进行许可。
