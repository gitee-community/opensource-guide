---
layout: home
hero:
  name: Git 团队协作与管理（进阶篇）
  tagline: 如何使用 Git 进行团队协作

features:
  - title: 分布式 Git
    details: 最常用到的 Git 命令都有哪些？
    link: /git-tutorial/advanced/distributed-of-git
  - title: Git 工具
    details: 如何使用命令进行分支与合并的操作
    link: /git-tutorial/advanced/tools
  - title: 定制 Git
    details: 使用命令在本地搭建 Git
    link: /git-tutorial/advanced/customization-of-git
  - title: Git 与其他系统
    details: 如何使用命令对远程仓库进行克隆、拉取、推送等操作？
    link: /git-tutorial/advanced/other-vcs
  - title: Git 内部原理
    details: 如何使用命令对远程仓库进行克隆、拉取、推送等操作？
    link: /git-tutorial/advanced/internal-principle
  - title: GitOps
    details: 如何使用命令对远程仓库进行克隆、拉取、推送等操作？
    link: /git-tutorial/advanced/gitpos
---