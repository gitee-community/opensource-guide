---
layout: home
hero:
  name: Git 教程
  tagline: 一份简单易懂的 Git 入门指导教程
  image: /git-icon.png
  actions:
    - text: 开始学习
      link: /git-tutorial/getting-started/

features:
  - title: Git 入门（初级篇）
    link: /git-tutorial/getting-started/what-is-git
  - title: Git 命令详解
    link: /git-tutorial/commands/git-commands
  - title: Git 团队协作与管理（进阶篇）
    link: /git-tutorial/advanced/distributed-of-git/distributed-workflow
---

## 🎩 编写人员

@小熊

@willcoder

@Gitees Urgroßvater

@阿基米东
