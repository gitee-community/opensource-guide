---
layout: home
hero:
  name: 开源指北
  tagline: 开源新手的保姆级开源百科
  actions:
    - text: 开始阅读
      link: '/guide/introduction/what-is/'
features:
  - title: 第一部分：初识开源
    link: '/guide/introduction/what-is/'
  - title: 第二部分：学习和使用开源项目
    link: '/guide/learning-and-using/find-projects/'
  - title: 第三部分：尝试参与开源
    link: '/guide/participating/roles/'
  - title: 第四部分：启动自己的开源项目
    link: '/guide/starting-your-own/where-to-start/'
  - title: 第五部分：开源治理
    link: '/guide/governance/individual-or-community/'
  - title: 第六部分：有关开源的其他问题
    link: '/guide/others/balancing/'
---